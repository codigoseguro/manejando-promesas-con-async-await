/** @format */

/**
 * El mundo de fantasía donde todo sale bien.
 */

async function sumarAleatorios() {
	const sumando1 = await obtenerNumeroAleatorio();
	const sumando2 = await obtenerNumeroAleatorio();
	const resultadoSuma = sumar(sumando1, sumando2);
	console.log(`El resultado de ${sumando1} + ${sumando2} es ${resultadoSuma}`);
	document.getElementById('resultado').innerHTML = resultadoSuma;
}

function obtenerNumeroAleatorio() {
	return new Promise((resolve) => {
		setTimeout(() => {
			const numeroAleatorio = Math.floor(Math.random() * 10) + 1;
			console.log(`El número aleatorio generado es: ${numeroAleatorio}`);
			resolve(numeroAleatorio);
		}, 2500);
	});
}

function sumar(a, b) {
	return a + b;
}

/**
 * Manejando excepciones.
 */

function alzarErrores() {
	alzarErroresAsync()
		.then()
		.catch((error) => console.log(error));
}

async function alzarErroresAsync() {
	const resultado1 = await promesaRechazada('asdasd');
	console.log('No llego a ejecutame.');
}

function promesaRechazada(parametro) {
	return new Promise((resolve, reject) => {
		reject(new Error(`No he podido resolver: ${parametro}`));
	});
}
